import React, {useEffect, useState} from 'react';
import {
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  View,
  Image,
} from 'react-native';

import {VideoModel} from './VideoModel';

const App: () => React$Node = () => {

  
  const [showModal, setShowModal] = useState({isVisible: false, data: null});
const [videos,setVideos]=useState([]);

  const toggleModal = state => {
console.log('state  ',state);

   setShowModal({
      isVisible: state.isVisible,
      data: state,
    }); 
  };
  //https://www.youtube.com/watch?v=rvRyXhjQ0C8
//https://www.mediafire.com/file/vuxhn73malcin7w/macOS+Big+Sur+Beta+11.0+(20A5343j).iso/file
//https://medium.com/@rishi.sahu20/react-native-video-fullscreen-control-ca427e6a3a17
  
const videosData=[
{
  url:'http://www.joseisama.de/AnimationWithSong.mp4',
  title:'Basic one',
},
{
  url:'http://www.joseisama.de/basicSalsa.mp4',
  title:'Basic tow',
},
{
  url:'http://www.joseisama.de/basicSalsa.mp4',
  title:'Basic three',
}

];



useEffect(()=>{
  setVideos(videosData);

  console.log(videosData.length,' videosData  ',videosData);
},[]);




  return (
    <View style={styles.container}>
<Text>swedede</Text>


    {showModal.isVisible ? (
      <VideoModel
        isVisible={showModal.isVisible}
        toggleModal={toggleModal}
        videoDetail={showModal.data}
        
      />
    ) : null}
    <View style={styles.videoContainer}>
      {videos && (
        <FlatList
          data={videos}
          renderItem={({item}) => (
            <View style={styles.VideoThumbWrapper}>
              <TouchableOpacity
                onPress={() => {
                  setShowModal({
                    isVisible: true,
                    data: item,
                  });
                }}>
                <View style={styles.PlayIconContainer}>
                  <View style={styles.PlayIconWrapper}>
                    {/* <PlayIcon width={28} height={28} /> */}
                  </View>
                </View>
                <Image
                  style={styles.BackGroundImage}
                  source={item.thumbnail}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
              <Text >{item.title}</Text>
            </View>
          )}
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />
      )}
    </View>
  </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingTop: 30,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  videoContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    paddingVertical: 2,
    paddingHorizontal: 6,
  },
  ModalOutsideContainer: {
    flex: 1,
  },
  VideoName: {
    paddingVertical: 1,
    paddingHorizontal: 2,
    fontSize: 16,
  },
  VideoThumbWrapper: {
    position: 'relative',
    width: '48%',
    marginRight: 8,
    marginBottom: 4,
  },
  PlayIconContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1,
  },
  PlayIconWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  BackGroundImage: {
    width: '100%',
    height: 100,
    justifyContent: 'center',
  },
  ModalContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    marginTop: '25%',
  },
  ModalWrapper: {
    flex: 1,
  },
  ModalBox: {
    width: '85%',
    backgroundColor: '#fff',
    paddingVertical: 2,
    paddingHorizontal: 6,
    borderRadius: 2,
    opacity: 1,
  },
  VideoPlayerContainer: {
    width: '100%',
    height: 150,
  },
  VideoTitle: {
    paddingTop: 8,
    fontSize: 18,
  },
});

export default App;
